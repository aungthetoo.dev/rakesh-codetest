import { createAsyncThunk } from "@reduxjs/toolkit";
import { API } from ".";

export const fetchFeedAction = createAsyncThunk(
  "feed/list",
  async (payload, { rejectWithValue }) => {
    try {
      const { limit, offset, append } = payload;
      const { data } = await API.get(
        `/articles?limit=${limit}&offset=${offset}`
      );
      return { data: data.articles, append: !!append };
    } catch (e) {
      rejectWithValue("Error on loading feed");
    }
  }
);
