import { StyleService } from "@ui-kitten/components";

export const postActionStyles = StyleService.create({
  container: {},
  reactionContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 4,
    gap: 32,
  },
  commentContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginVertical: 8,
    gap: 16,
  },
  reaction: {
    flexDirection: "row",
    alignItems: "center",
    gap: 4,
  },
  commentBox: {
    flex: 1,
    borderRadius: 16,
  },
  commentActionsContainer: {
    flexDirection: "row",
    gap: 8,
  },
  userImage: {
    width: 32,
    height: 32,
    borderRadius: 16,
  },
});
