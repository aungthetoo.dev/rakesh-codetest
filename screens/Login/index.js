import { Alert, ScrollView, View } from "react-native";
import React, { useState } from "react";
import {
  Button,
  Card,
  Divider,
  Input,
  Text,
  useStyleSheet,
} from "@ui-kitten/components";
import { loginStyles } from "./styles";
import { useDispatch, useSelector } from "react-redux";
import { userActions } from "../../redux/slices/userSlice";
import { signIn } from "../../redux/actions/userActions";

const Login = () => {
  const styles = useStyleSheet(loginStyles);
  const dispatch = useDispatch();
  const { data, loading, error } = useSelector((state) => state.user);

  const [user, setUser] = useState({
    email: null,
    password: null,
  });

  const handleUserSignIn = () => {
    const { email, password } = user;
    if (!email || !password)
      return Alert.alert("Missing Info", "Please fill your email & password");
    dispatch(signIn(user));
  };

  return (
    <ScrollView
      contentContainerStyle={styles.container}
      bounces={false}
      showsVerticalScrollIndicator={false}
    >
      <Card
        disabled
        header={() => (
          <Text style={styles.loginTitle} category="s1">
            Welcome
          </Text>
        )}
        footer={() => (
          <View style={styles.footerContainer}>
            <Button disabled={loading} onPress={handleUserSignIn}>
              Login Now
            </Button>
            {error ? (
              <Text style={styles.errorText} category="c1" status="danger">
                {error}
              </Text>
            ) : null}
          </View>
        )}
      >
        <Input
          keyboardType="email-address"
          style={styles.inputBox}
          label="Email"
          autoCapitalize="none"
          value={user.email}
          onChangeText={(email) => setUser({ ...user, email })}
        />
        <Input
          secureTextEntry
          style={styles.inputBox}
          label="Password"
          value={user.password}
          onChangeText={(password) => setUser({ ...user, password })}
        />
      </Card>
      <Divider style={styles.divider} />
      <Button
        onPress={() => dispatch(userActions.loginAsGuest({ name: "Guest" }))}
        appearance="outline"
      >
        Continue as Guest
      </Button>
    </ScrollView>
  );
};

export default Login;
