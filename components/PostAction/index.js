import { Alert, Image, Pressable, TouchableOpacity, View } from "react-native";
import React, { useState } from "react";
import { Input, Text, useStyleSheet } from "@ui-kitten/components";
import { postActionStyles } from "./styles";
import { FontAwesome5, Ionicons } from "@expo/vector-icons";
import { useSelector } from "react-redux";
import { API } from "../../redux/actions";
import { useNavigation } from "@react-navigation/native";

const PostAction = ({ favoritesCount, slug }) => {
  const styles = useStyleSheet(postActionStyles);
  const { isGuest, data } = useSelector((state) => state.user);
  const navigation = useNavigation();

  const [reaction, setReaction] = useState({
    liked: false,
    shared: false,
    comment: "",
  });

  const handleReaction = () => {
    if (isGuest) {
      return Alert.alert(
        "Need Account",
        "you must signup first to react this feed"
      );
    }

    setReaction((prev) => ({ ...reaction, liked: !prev.liked }));
  };

  const handleShare = () => {
    if (isGuest) {
      return Alert.alert(
        "Need Account",
        "you must signup first to react this feed"
      );
    }

    setReaction((prev) => ({ ...reaction, shared: !prev.shared }));
  };

  const handleCommentSubmision = async () => {
    if (isGuest) {
      return Alert.alert(
        "Need Account",
        "you must signup first to react this feed"
      );
    }

    try {
      if (reaction.comment.trim() == "") return;

      const { data: commentResponse } = await API.post(
        `/articles/${slug}/comments`,
        {
          comment: { body: reaction.comment },
        },
        {
          headers: {
            Authorization: `Bearer ${data.token}`,
          },
        }
      );
      setReaction({ ...reaction, comment: "" });
    } catch (e) {
      Alert.alert("Oops", "Error on posting your comment, try later");
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.reactionContainer}>
        <Pressable style={styles.reaction} onPress={handleReaction}>
          <Ionicons
            name={reaction.liked ? "heart" : "heart-outline"}
            size={24}
            color="#54617B"
          />
          <Text category="c1">
            {reaction.liked ? favoritesCount + 1 : favoritesCount}
          </Text>
        </Pressable>

        <Pressable
          style={styles.reaction}
          onPress={() => navigation.navigate("feed-detail", { slug })}
        >
          <Ionicons name="chatbubble" size={24} color="#54617B" />
        </Pressable>

        <Pressable style={styles.reaction} onPress={handleShare}>
          <Ionicons name="share-social" size={24} color="#54617B" />
          <Text category="c1">{reaction.shared ? 1 : 0}</Text>
        </Pressable>
      </View>
      <View style={styles.commentContainer}>
        {isGuest ? (
          <FontAwesome5 name="user-circle" size={28} color="#54617B" />
        ) : (
          <Image source={{ uri: data.image }} style={styles.userImage} />
        )}

        <Input
          value={reaction.comment}
          onChangeText={(comment) => setReaction({ ...reaction, comment })}
          style={styles.commentBox}
          accessoryRight={() =>
            reaction.comment.trim() == "" ? (
              <View style={styles.commentActionsContainer}>
                <TouchableOpacity activeOpacity={0.5}>
                  <Ionicons name="image-outline" size={24} color="#54617B" />
                </TouchableOpacity>

                <TouchableOpacity activeOpacity={0.5}>
                  <Ionicons name="gift-outline" size={24} color="#54617B" />
                </TouchableOpacity>

                <TouchableOpacity activeOpacity={0.5}>
                  <Ionicons
                    name="play-circle-outline"
                    size={24}
                    color="#54617B"
                  />
                </TouchableOpacity>
              </View>
            ) : (
              <View style={styles.commentActionsContainer}>
                <TouchableOpacity
                  activeOpacity={0.5}
                  onPress={handleCommentSubmision}
                >
                  <Ionicons name="send-outline" size={24} color="#fff" />
                </TouchableOpacity>
              </View>
            )
          }
        />
      </View>
    </View>
  );
};

export default PostAction;
