import { View } from "react-native";
import React from "react";
import { Text, useStyleSheet } from "@ui-kitten/components";
import { postContentStyles } from "./styles";

const Tag = ({ tags }) => {
  const styles = useStyleSheet(postContentStyles);
  return (
    <View style={styles.tagContainer}>
      {tags.map((tag) => (
        <Text key={tag} category="s2" status="info" style={styles.tagContent}>
          @{tag}
        </Text>
      ))}
    </View>
  );
};

export default Tag;
