import { createAsyncThunk } from "@reduxjs/toolkit";
import { API, parseErrorResponse } from ".";

export const signIn = createAsyncThunk(
  "user/signIn",
  async (payload, { rejectWithValue }) => {
    try {
      const { data } = await API.post("/users/login", { user: payload });
      return data.user;
    } catch (e) {
      let error = "Unknown";

      if (e.response) {
        error = parseErrorResponse(e.response.data.errors);
      }

      return rejectWithValue(error);
    }
  }
);
