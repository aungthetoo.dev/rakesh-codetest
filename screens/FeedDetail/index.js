import { ScrollView, View } from "react-native";
import React, { useEffect, useState } from "react";
import { Button, Text, useStyleSheet } from "@ui-kitten/components";
import { feedDetailStyles } from "./styles";
import Post from "../../components/Post";
import { API } from "../../redux/actions";
import { ActivityIndicator } from "react-native";
import { useSelector } from "react-redux";
import Comments from "../../components/Comments";

const FeedDetail = ({ navigation, route }) => {
  const { slug, title } = route.params;
  const style = useStyleSheet(feedDetailStyles);

  const [feed, setFeed] = useState({
    loading: false,
    data: null,
    error: null,
  });

  const fetchPostBySlug = async () => {
    try {
      setFeed({ ...feed, loading: true, error: null, data: null });
      const { data } = await API.get(`/articles/${slug}`);
      setFeed({ ...feed, loading: false, error: null, data: data.article });
    } catch (e) {
      setFeed({ ...feed, error: "Error on retriving the feed" });
    }
  };

  useEffect(() => {
    // UI not so good
    // navigation.setOptions({
    //   title,
    // });
    fetchPostBySlug();
  }, [slug]);

  if (feed.loading) {
    return (
      <View style={[style.container, { justifyContent: "center" }]}>
        <ActivityIndicator color="#fff" />
      </View>
    );
  }

  if (feed.error) {
    return (
      <View
        style={[
          style.container,
          { justifyContent: "center", alignItems: "center", gap: 16 },
        ]}
      >
        <Text>Error on retriving the feed</Text>
        <Button size="tiny" onPress={() => fetchPostBySlug()}>
          Refresh
        </Button>
      </View>
    );
  }

  return (
    <ScrollView style={style.container}>
      {feed.data ? <Post data={feed.data} /> : null}
      <Comments slug={slug} />
    </ScrollView>
  );
};

export default FeedDetail;
