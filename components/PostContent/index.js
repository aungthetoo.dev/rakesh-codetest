import { Pressable, View } from "react-native";
import React, { memo, useState } from "react";
import { Text, useStyleSheet } from "@ui-kitten/components";
import { postContentStyles } from "./styles";
import { Image } from "expo-image";
import Tag from "./Tag";

const PostContent = ({ tags, body, title }) => {
  const styles = useStyleSheet(postContentStyles);
  const [showDetail, setShowDetail] = useState(false);

  return (
    <View style={styles.container}>
      <Text category="c2">{title}</Text>

      {showDetail && (
        <>
          <Text category="c2" style={styles.bodyContent}>
            {body}
          </Text>
          <Tag tags={tags} />
        </>
      )}
      <Pressable onPress={() => setShowDetail((prev) => !prev)}>
        <Text status="info" category="s2">
          {showDetail ? "read less..." : "read more..."}
        </Text>
      </Pressable>
      <Image
        style={styles.image}
        source="https://picsum.photos/1000/600"
        contentFit="fill"
        transition={1000}
      />
    </View>
  );
};

export default memo(PostContent);
