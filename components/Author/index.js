import { Image, View } from "react-native";
import React from "react";
import { Text, useStyleSheet } from "@ui-kitten/components";
import { authorStyles } from "./styles";
import { FontAwesome } from "@expo/vector-icons";
import moment from "moment";

const Author = ({ author, date }) => {
  const styles = useStyleSheet(authorStyles);
  return (
    <View style={styles.container}>
      <Image
        source={{ uri: author.image }}
        style={styles.authorImage}
        resizeMode="contain"
      />
      <View style={styles.authorInfo}>
        <View style={styles.author}>
          <Text category="s1">{author.username}</Text>
          <Text category="c1">{moment(date).fromNow(true)}</Text>
        </View>
        <View style={styles.followerContainer}>
          <Text category="c1">52 followers</Text>
          <FontAwesome name="circle" size={4} color="#fff" />
          <Text category="c1">10 following</Text>
        </View>
      </View>
    </View>
  );
};

export default Author;
