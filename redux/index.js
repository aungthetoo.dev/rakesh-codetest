const { configureStore, combineReducers } = require("@reduxjs/toolkit");
import { persistReducer, persistStore } from "redux-persist";
import { userReducer } from "./slices/userSlice";
import thunk from "redux-thunk";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { feedReducer } from "./slices/feedSlice";

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
};

const rootReducer = combineReducers({
  user: userReducer,
  feed: feedReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false }).concat(thunk),
});

export const persistor = persistStore(store);

// persistor.purge();
