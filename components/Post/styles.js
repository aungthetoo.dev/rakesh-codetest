import { StyleService } from "@ui-kitten/components";

export const postStyles = StyleService.create({
  container: {
    marginBottom: 8,
    backgroundColor: "#141820",
    padding: 8,
  },
});
