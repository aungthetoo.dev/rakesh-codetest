import { StyleService } from "@ui-kitten/components";

export const loginStyles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: "#07080B",
    justifyContent: "center",
    paddingHorizontal: 16,
  },
  loginTitle: {
    padding: 16,
    backgroundColor: "color-info-400",
    color: "#fff",
  },
  footerContainer: {
    padding: 16,
  },
  inputBox: {
    marginBottom: 16,
  },
  divider: {
    marginVertical: 32,
    backgroundColor: "#AAA",
  },
  errorText: {
    textAlign: "center",
    lineHeight: 36,
  },
});
