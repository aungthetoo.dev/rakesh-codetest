import { signIn } from "../actions/userActions";

const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  data: null,
  loading: false,
  error: null,
  isGuest: false,
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    logOutUser: (state) => {
      state.data = null;
      state.isGuest = false;
    },
    loginAsGuest: (state, actions) => {
      state.data = actions.payload;
      state.isGuest = true;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(signIn.pending, (state, actions) => {
      state.loading = true;
      state.error = null;
      state.data = null;
    });

    builder.addCase(signIn.fulfilled, (state, actions) => {
      state.loading = false;
      state.error = null;
      state.data = actions.payload;
    });

    builder.addCase(signIn.rejected, (state, actions) => {
      state.loading = false;
      state.error = actions.payload;
      state.data = null;
    });
  },
});

export const { reducer: userReducer, actions: userActions } = userSlice;
