import { StyleService } from "@ui-kitten/components";

export const postInputStyles = StyleService.create({
  container: {
    backgroundColor: "#0D0F14",
    padding: 16,
    borderRadius: 8,
    marginBottom: 16,
  },
  title: {
    fontSize: 14,
    color: "#fff",
  },
  postInputContainer: {
    marginTop: 16,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    gap: 8,
  },
  postInputBox: {
    flex: 1,
    borderRadius: 8,
    backgroundColor: "#0D0F14",
  },
  userImage: {
    width: 32,
    height: 32,
    borderRadius: 16,
  },
});
