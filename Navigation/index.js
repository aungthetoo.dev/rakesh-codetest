import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Login from "../screens/Login";
import Feed from "../screens/Feed";
import { useDispatch, useSelector } from "react-redux";
import FeedDetail from "../screens/FeedDetail";
import { Ionicons } from "@expo/vector-icons";
import { Pressable } from "react-native";
import { userActions } from "../redux/slices/userSlice";

const Stack = createStackNavigator();

const AppMainStackNavigator = () => {
  const dispatch = useDispatch();
  return (
    <Stack.Navigator
      screenOptions={{
        headerRight: () => {
          return (
            <Pressable
              onPress={() => dispatch(userActions.logOutUser())}
              style={{ marginHorizontal: 8 }}
            >
              <Ionicons name="log-out" size={24} color="#fff" />
            </Pressable>
          );
        },
      }}
    >
      <Stack.Screen
        name="feed"
        component={Feed}
        options={{
          title: "FEED",
          headerTintColor: "#fff",
          headerStyle: {
            backgroundColor: "#07080B",
          },
        }}
      />
      <Stack.Screen
        name="feed-detail"
        component={FeedDetail}
        options={{
          title: null,
          headerTintColor: "#fff",
          headerBackTitleVisible: false,
          headerStyle: {
            backgroundColor: "#07080B",
          },
        }}
      />
    </Stack.Navigator>
  );
};
const AppAuthStackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="login"
        component={Login}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

export const Navigation = () => {
  const { data } = useSelector((state) => state.user);
  return (
    <NavigationContainer>
      {data ? <AppMainStackNavigator /> : <AppAuthStackNavigator />}
    </NavigationContainer>
  );
};
