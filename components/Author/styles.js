import { StyleService } from "@ui-kitten/components";

export const authorStyles = StyleService.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    gap: 16,
  },
  authorInfo: {
    gap: 4,
  },
  author: {
    flexDirection: "row",
    alignItems: "center",
    gap: 8,
  },

  authorImage: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },
  followerContainer: {
    flexDirection: "row",
    alignItems: "center",
    gap: 8,
  },
});
