import { StyleService } from "@ui-kitten/components";

export const postContentStyles = StyleService.create({
  container: {
    marginVertical: 8,
  },
  bodyContent: {
    marginVertical: 4,
  },
  image: {
    width: "100%",
    height: 230,
    marginTop: 16,
    borderRadius: 8,
  },
  tagContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    gap: 16,
    marginVertical: 8,
  },
  tagContent: {
    backgroundColor: "#172339",
    padding: 4,
  },
});
