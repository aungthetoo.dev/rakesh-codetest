import { StyleService } from "@ui-kitten/components";

export const feedDetailStyles = StyleService.create({
  container: {
    flex: 1,
    padding: 8,
    backgroundColor: "#07080B",
  },
});
