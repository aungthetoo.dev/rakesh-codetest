import { Image, Pressable, View } from "react-native";
import React from "react";
import { Input, Text, useStyleSheet } from "@ui-kitten/components";
import { postInputStyles } from "./styles";
import { FontAwesome5 } from "@expo/vector-icons";
import { useSelector } from "react-redux";
const PostInput = ({ user }) => {
  const styles = useStyleSheet(postInputStyles);
  const { data, isGuest } = useSelector((state) => state.user);
  return (
    <View style={styles.container}>
      <Text category="h6" style={styles.title}>
        Create a Post
      </Text>
      <View style={styles.postInputContainer}>
        {isGuest ? (
          <FontAwesome5 name="user-circle" size={24} color="#54617B" />
        ) : (
          <Image source={{ uri: data.image }} style={styles.userImage} />
        )}

        <Input
          size="small"
          style={styles.postInputBox}
          placeholder="Your thoughts on the crypto market..."
        />
        <Pressable>
          <FontAwesome5 name="plus-square" size={24} color="#5ACEFF" />
        </Pressable>
      </View>
    </View>
  );
};

export default PostInput;
