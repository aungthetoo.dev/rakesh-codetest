import { StyleService } from "@ui-kitten/components";

export const commentStyles = StyleService.create({
  container: {
    marginVertical: 16,
  },
  errorContainer: {
    gap: 16,
    justifyContent: "center",
    alignItems: "center",
  },
  commentInfoContainer: {
    width: "100%",
    minHeight: 50,
    marginBottom: 32,
    gap: 8,
  },
  commentContent: {
    backgroundColor: "#141820",
    padding: 8,
    width: "90%",
    alignSelf: "flex-end",
    position: "relative",
  },
  deleteCommentContainer: {
    position: "absolute",
    right: 5,
    top: -10,
    width: 25,
    height: 25,
    borderRadius: 25 / 2,
    backgroundColor: "#333",
    justifyContent: "center",
    alignItems: "center",
  },
});
