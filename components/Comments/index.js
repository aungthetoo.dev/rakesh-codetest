import { ActivityIndicator, Alert, Pressable, View } from "react-native";
import React, { useEffect, useState } from "react";
import { Text, useStyleSheet, Button } from "@ui-kitten/components";
import { commentStyles } from "./styles";
import Author from "../Author";
import { API } from "../../redux/actions";
import { useSelector } from "react-redux";
import { Ionicons } from "@expo/vector-icons";

const Comments = ({ slug }) => {
  const styles = useStyleSheet(commentStyles);
  const { data: user, isGuest } = useSelector((state) => state.user);

  const [comments, setComments] = useState({
    loading: false,
    data: [],
    error: null,
  });

  const fetchCommentsBySlug = async () => {
    try {
      setComments({
        ...comments,
        loading: true,
        error: null,
      });

      const { data } = await API.get(`/articles/${slug}/comments`, {
        headers: { Authorization: `Bearer ${user.token}` },
      });

      setComments({
        ...comments,
        data: data.comments,
      });
    } catch (e) {
      setComments({ ...comments, error: "Error on retriving the comments" });
    }
  };

  const deleteComments = async (id) => {
    try {
      setComments({
        ...comments,
        loading: true,
        error: null,
      });

      await API.delete(`/articles/${slug}/comments/${id}`, {
        headers: { Authorization: `Bearer ${user.token}` },
      });

      fetchCommentsBySlug();
    } catch (e) {
      console.log(e);
      alert("can't delete the comment");
    }
  };

  const confirmDeleteComment = (id) => {
    Alert.alert("Delete Comment", "you are going to delete this comment", [
      {
        text: "No",
        style: "cancel",
      },
      {
        text: "Delete",
        style: "destructive",
        onPress: () => deleteComments(id),
      },
    ]);
  };

  useEffect(() => {
    !isGuest && fetchCommentsBySlug();
  }, [slug]);

  if (comments.loading) {
    return (
      <View style={styles.errorContainer}>
        <ActivityIndicator />
      </View>
    );
  }

  if (comments.error) {
    return (
      <View style={styles.errorContainer}>
        <Text category="c2">
          {comments.error || "Error on retriving the comments"}
        </Text>
        <Button onPress={fetchCommentsBySlug} size="tiny">
          Refresh
        </Button>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      {comments.data.map((comment) => (
        <View key={comment.id} style={styles.commentInfoContainer}>
          <Author author={comment.author} date={comment.createdAt} />
          <View style={styles.commentContent}>
            <Text>{comment.body}</Text>
            {user.username == comment.author.username ? (
              <Pressable
                onPress={() => confirmDeleteComment(comment.id)}
                style={styles.deleteCommentContainer}
              >
                <Ionicons name="trash" size={16} color="#fff" />
              </Pressable>
            ) : null}
          </View>
        </View>
      ))}
    </View>
  );
};

export default Comments;
