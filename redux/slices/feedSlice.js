import { createSlice } from "@reduxjs/toolkit";
import { fetchFeedAction } from "../actions/feedActions";

const initialState = {
  data: [],
  loading: false,
  error: null,
};

const feedSlice = createSlice({
  name: "feed",
  initialState,
  extraReducers: (builder) => {
    builder.addCase(fetchFeedAction.pending, (state, actions) => {
      state.loading = true;
      state.error = null;
    });

    builder.addCase(fetchFeedAction.fulfilled, (state, actions) => {
      const { data, append } = actions.payload;
      state.loading = false;
      state.error = null;

      if (append) {
        state.data = [...state.data, ...data];
      } else {
        state.data = data;
      }
    });

    builder.addCase(fetchFeedAction.rejected, (state, actions) => {
      state.loading = false;
      state.error = actions.payload;
    });
  },
});

export const { reducer: feedReducer, actions: feedActions } = feedSlice;
