import axios from "axios";

export const API = axios.create({
  baseURL: "https://api.realworld.io/api/",
});

export const parseErrorResponse = (errors) => {
  const errorResponse = Object.keys(errors).map((error) => {
    return `${error} ${errors[error]}`;
  });
  return errorResponse.join(" ");
};
