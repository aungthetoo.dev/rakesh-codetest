import { View, Text, Pressable } from "react-native";
import React from "react";
import { useStyleSheet } from "@ui-kitten/components";
import { postStyles } from "./styles";
import Author from "../Author";
import PostContent from "../PostContent";
import PostAction from "../PostAction";
import { useNavigation } from "@react-navigation/native";

const Post = ({ data }) => {
  const styles = useStyleSheet(postStyles);
  const navigation = useNavigation();
  const {
    author,
    createdAt,
    tagList: tags,
    title,
    slug,
    body,
    favoritesCount,
  } = data;
  return (
    <Pressable
      style={styles.container}
      onPress={() => navigation.navigate("feed-detail", { slug, title })}
    >
      <Author {...{ author }} date={createdAt} />
      <PostContent {...{ tags, title, body }} />
      <PostAction {...{ favoritesCount, slug }} />
    </Pressable>
  );
};

export default Post;
