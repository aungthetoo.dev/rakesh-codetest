import { AppState, RefreshControl, ScrollView, View } from "react-native";
import React, { useEffect, useRef } from "react";
import { Button, Text, useStyleSheet } from "@ui-kitten/components";
import { feedStyles } from "./styles";
import PostInput from "../../components/PostInput";
import Post from "../../components/Post";
import { FlashList } from "@shopify/flash-list";
import { useDispatch, useSelector } from "react-redux";
import { Ionicons } from "@expo/vector-icons";
import { fetchFeedAction } from "../../redux/actions/feedActions";

const Feed = () => {
  const styles = useStyleSheet(feedStyles);
  const { data, loading, error } = useSelector((state) => state.feed);
  const dispatch = useDispatch();

  const page = useRef(0);
  const onMomentumScrollBegin = useRef(true);
  const appState = useRef(AppState.currentState);
  const scrollRef = useRef();

  useEffect(() => {
    dispatch(fetchFeedAction({ limit: 10, offset: 0 }));
  }, []);

  useEffect(() => {
    const subscription = AppState.addEventListener("change", (state) => {
      if (appState.current.match(/inactive|background/) && state === "active") {
        dispatch(fetchFeedAction({ limit: 10, offset: 0 }));
        page.current = 0;
        scrollRef.current?.scrollToIndex({
          animated: true,
          index: 0,
        });
      }
      appState.current = state;
    });

    return () => subscription.remove();
  }, []);

  const onRefresh = () => dispatch(fetchFeedAction({ limit: 10, offset: 0 }));

  const onEndReached = () => {
    page.current++;
    dispatch(
      fetchFeedAction({ limit: 10, offset: page.current, append: true })
    );
  };

  return (
    <View style={styles.container}>
      <PostInput />
      {error ? (
        <View style={styles.bugContainer}>
          <Ionicons name="bug-outline" size={32} color="#cecece" />
          <Text category="c1">Something wrong, please try again</Text>
          <Button
            size="tiny"
            appearance="outline"
            style={styles.refreshButton}
            onPress={onRefresh}
          >
            Refresh
          </Button>
        </View>
      ) : (
        <FlashList
          data={data}
          ref={scrollRef}
          refreshControl={
            <RefreshControl
              refreshing={loading}
              onRefresh={onRefresh}
              colors={["#fff"]}
              tintColor="#fff"
            />
          }
          estimatedItemSize={200}
          renderItem={({ item }) => <Post data={item} />}
          showsVerticalScrollIndicator={false}
          onEndReachedThreshold={0.5}
          onMomentumScrollBegin={() => (onMomentumScrollBegin.current = false)}
          onEndReached={() => {
            if (!onMomentumScrollBegin.current) {
              onEndReached();
              onMomentumScrollBegin.current = true;
            }
          }}
          ListEmptyComponent={() => (
            <View style={styles.noFeedContainer}>
              <Text category="c1">Loading...</Text>
            </View>
          )}
        />
      )}
    </View>
  );
};

export default Feed;
