import { StyleService } from "@ui-kitten/components";

export const feedStyles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: "#07080B",
  },
  noFeedContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  bugContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  refreshButton: {
    marginVertical: 8,
  },
});
